﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Launcher
{
    public static class Hyperlinks
    {
        public const string WebsiteURL = "https://vtolvr-mods.com";
        public const string DiscordURL = "https://discord.gg/XZeeafp";
        public const string DocsURL = "https://docs.vtolvr-mods.com";
        public const string PatreonURL = "https://www.patreon.com/vtolvrmods";
    }
}
