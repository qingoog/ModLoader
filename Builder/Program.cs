﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.IO.Compression;
using Builder;
using CommandLine;
using Console = Colorful.Console;

namespace Build
{
    class Program
    {
        static void Main(string[] args)
        {
            Parser.Default.ParseArguments<BuildDll, BuildWpf, BuildPatcher, BuildUnPatcher, Package>(args)
                .MapResult(
                    (BuildDll opts) => RunBuildDll(opts),
                    (BuildWpf opts) => RunBuildWpf(opts),
                    (Builder.BuildPatcher opts) => RunBuildPatcher(opts),
                    (Builder.BuildUnPatcher opts) => RunBuildUnPatcher(opts),
                    (Package opts) => RunPackage(opts),
                    errs => 1);
            return;
        }

        private static int RunPackage(Package opts)
        {
            Console.WriteLine("Creating zip for auto updater");

            if (File.Exists(opts.OutputPath))
            {
                var file = new FileInfo(opts.OutputPath);
                Console.WriteLine($"Deleting {file.Name}", Color.Yellow);
                file.Delete();
            }

            var autoUpdatePath = Path.Combine(opts.ModLoaderRepo, "autoupdate");
            if (Directory.Exists(autoUpdatePath))
            {
                var autoUpdateDir = new DirectoryInfo(autoUpdatePath);
                Console.WriteLine($"Deleting {autoUpdateDir.FullName}", Color.Yellow);
                autoUpdateDir.Delete(true);
            }

            //Copy all folders
            foreach (string dirPath in Directory.GetDirectories(opts.TemplatePath, "*",
                SearchOption.AllDirectories))
            {
                string directory = dirPath.Replace(opts.TemplatePath,  Path.Combine(opts.ModLoaderRepo, "autoupdate", "template"));
                Console.WriteLine($"Creating folder: {directory}");
                Directory.CreateDirectory(directory);
            }
            //Copy all files
            foreach (string newPath in Directory.GetFiles(opts.TemplatePath, "*.*",
                SearchOption.AllDirectories))
            {
                string filePath = newPath.Replace(opts.TemplatePath, Path.Combine(opts.ModLoaderRepo, "autoupdate", "template"));
                Console.WriteLine($"Copying file from {newPath} to {filePath}");
                File.Copy(newPath, filePath, true);
            }

            Console.WriteLine("Creating Default folders");
            Directory.CreateDirectory(opts.ModLoaderRepo + @"\autoupdate\template\VTOLVR_Data\Managed");
            Directory.CreateDirectory(opts.ModLoaderRepo + @"\autoupdate\template\VTOLVR_Data\Plugins");
            Directory.CreateDirectory(opts.ModLoaderRepo + @"\autoupdate\template\VTOLVR_ModLoader\mods");
            Directory.CreateDirectory(opts.ModLoaderRepo + @"\autoupdate\template\VTOLVR_ModLoader\skins");

            Console.WriteLine("Moving Applications");
            File.Copy(
                Path.Combine(opts.ModLoaderRepo,"Core", "bin", "Release", "netstandard2.0", "Core.dll"), 
                Path.Combine(opts.ModLoaderRepo,"autoupdate","template", "VTOLVR_Data","Managed", "Core.dll"));
            File.Copy(
                Path.Combine(opts.ModLoaderRepo,"ModLoader", "bin", "Release", "ModLoader.dll"), 
                Path.Combine(opts.ModLoaderRepo,"autoupdate","template", "VTOLVR_ModLoader","ModLoader.dll"));
            File.Copy(
                Path.Combine(opts.ModLoaderRepo,"ModLoader", "bin", "Release", "ModLoader.xml"), 
                Path.Combine(opts.ModLoaderRepo,"autoupdate","template", "VTOLVR_ModLoader","ModLoader.xml"));
            File.Copy(
                Path.Combine(opts.ModLoaderRepo,"VTPatcher", "bin", "Release","VTPatcher.dll"),
                Path.Combine(opts.ModLoaderRepo,"autoupdate","template", "VTOLVR_ModLoader","VTPatcher.dll"));
            File.Copy(
                Path.Combine(opts.ModLoaderRepo,"UnPatcher",  "bin", "Release","UnPatcher.exe"), 
                Path.Combine(opts.ModLoaderRepo,"autoupdate","template", "VTOLVR_ModLoader","UnPatcher.exe"));
            File.Copy(
                Path.Combine(opts.ModLoaderRepo,"Launcher", "bin", "Release", "net5.0-windows", "win-x64", "publish", "Launcher.exe"), 
                Path.Combine(opts.ModLoaderRepo,"autoupdate","template", "VTOLVR_ModLoader","VTOLVR-ModLoader.exe"));

            Console.WriteLine("Creating zip");
            ZipFile.CreateFromDirectory(opts.ModLoaderRepo + @"\autoupdate\", opts.OutputPath);
            return 0;
        }

        private static int RunBuildUnPatcher(BuildUnPatcher opts)
        {
            if (!string.IsNullOrWhiteSpace(opts.DllsPath))
            {
                MoveDlls(opts);
            }
            
            Console.WriteLine("Restoring UnPatcher.exe");
            Run(opts.NuGetPath, "restore", opts.ModLoaderRepo);
            Console.WriteLine("Building UnPatcher.exe");
            Run(opts.MsBuildPath, "-p:Configuration=Release -nologo \"UnPatcher.csproj\"", $@"{opts.ModLoaderRepo}\UnPatcher");
            Console.WriteLine("Finished building UnPatcher.exe", Color.Green);
            return 0;
        }

        private static int RunBuildPatcher(BuildPatcher opts)
        {
            if (!string.IsNullOrWhiteSpace(opts.DllsPath))
            {
                MoveDlls(opts);
            }
            
            Console.WriteLine("Restoring VTPatcher.dll");
            Run(opts.NuGetPath, "restore", opts.ModLoaderRepo);
            Console.WriteLine("Building VTPatcher.dll");
            Run(opts.MsBuildPath, "-p:Configuration=Release -nologo \"VTPatcher.csproj\"", @$"{opts.ModLoaderRepo}\VTPatcher");
            Console.WriteLine("Finished building VTPatcher.dll", Color.Green);
            return 0;
        }

        private static int RunBuildWpf(BuildWpf opts)
        {
            if (!string.IsNullOrWhiteSpace(opts.DllsPath))
            {
                MoveDlls(opts);
            }
            
            Console.WriteLine("Restoring VTOLVR-ModLoader.exe");
            Run(opts.NuGetPath, "restore", opts.ModLoaderRepo);
            Console.WriteLine("Building VTOLVR-ModLoader.exe");
            Run(opts.DotNetPath,
                "publish -r win-x64 --self-contained=true /p:PublishSingleFile=true /p:IncludeNativeLibrariesForSelfExtract=true -c Release",
                $@"{opts.ModLoaderRepo}\Launcher");
            var outputPath = Path.Combine(opts.OutputPath, "VTOLVR-ModLoader.exe");
            if (File.Exists(outputPath))
            {
                var file = new FileInfo(outputPath);
                Console.WriteLine($"Deleting {file.FullName}", Color.Yellow);
                file.Delete();
            }
            Console.WriteLine("Moving VTOLVR-ModLoader.exe");
            File.Copy(
                Path.Combine(opts.ModLoaderRepo,"Launcher", "bin", "Release", "net5.0-windows", "win-x64", "publish", "Launcher.exe"), 
                outputPath);
            Console.WriteLine("Finished publishing VTOLVR-ModLoader.exe", Color.Green);
            return 0;
        }

        private static int RunBuildDll(BuildDll opts)
        {
            if (!string.IsNullOrWhiteSpace(opts.DllsPath))
            {
                MoveDlls(opts);
            }
            
            Console.WriteLine("Restoring Core.dll");
            Run(opts.NuGetPath, "restore", opts.ModLoaderRepo);
            Console.WriteLine("Building Core.dll");
            Run(opts.MsBuildPath, "-p:Configuration=Release -nologo Core.csproj /t:Restore /t:Clean,Build ", @$"{opts.ModLoaderRepo}\Core");
            Console.WriteLine("Building ModLoader.dll");
            Run(opts.MsBuildPath, "-p:Configuration=Release;Documentationfile=bin\\Release\\ModLoader.xml -nologo \"Mod Loader.csproj\"", @$"{opts.ModLoaderRepo}\ModLoader");
            Console.WriteLine("Finished Building Core.dll & ModLoader.dll", Color.Green);
            return 0;
        }

        private static void MoveDlls(Code opts)
        {
            var newPath = @$"{opts.ModLoaderRepo}\dll";
            Console.WriteLine($"Moving Dlls from {opts.DllsPath} to {newPath}", Color.Yellow);
            string[] deps = Directory.GetFiles(opts.DllsPath, "*.dll", SearchOption.TopDirectoryOnly);
            for (int i = 0; i < deps.Length; i++)
            {
                File.Copy(deps[i], deps[i].Replace(opts.DllsPath, newPath), true);
            }

            Console.WriteLine($"Moved {deps.Length + 1} dependencies", Color.Green);
        }
        
        private static void Run(string file, string args, string workingDirectory)
        {
            Process process = new Process();
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = file;
            startInfo.Arguments = args;
            startInfo.UseShellExecute = false;
            startInfo.RedirectStandardOutput = true;
            startInfo.WorkingDirectory = workingDirectory;
            process.StartInfo = startInfo;
            process.Start();
            string output;
            while ((output = process.StandardOutput.ReadLine()) != null)
            {
                Console.WriteLine(output, Color.DimGray);
            }
            process.WaitForExit();
            if (process.ExitCode != 0)
                Environment.Exit(process.ExitCode);
        }
    }
}
